# Execução
O programa pode ser executado através do script $Trab4mc920.py$.
O script recebe como argumento uma imagem de entrada que se encontra no mesmo diretório que o arquivo, o algoritmo irá executar todas as transformações, salvar as imagens resultantes no mesmo diretório.
Para rodar o script podemos fazer dessa forma :

```python
python3 Trab4mc920.py bitmap.pbm
```

# output
O output será salvo na mesma pasta e com o seguinte nome :
[operação]\_[item],
sendo que [operação] pode variar entre os seguintes valores erosion, dilatation e componentes\_conexo, closing;
[item] isso indica qual o passo que está sendo executado no processo da imagem. 
O nome da imagem deve vir acompanhada do tipo, caso contrário, não será possível rodar o script e realizar a passagem de todos os filtros.
Além disso, o script gera dois arquivos texto, um sobre a contagem de pixels pretos para cada label e outro sobre as transições no eixo x e y para cada label.