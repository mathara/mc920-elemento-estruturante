#!/usr/bin/python
import cv2
import sys
import numpy as np 

#plotar a imagem para a depuração da operação
def plota_imagem(img):
	window_name = 'image'
	cv2.namedWindow(window_name,cv2.WINDOW_NORMAL)
	cv2.resizeWindow(window_name,(500,500))
	cv2.imshow(window_name,img)
	cv2.waitKey(0)
	cv2.destroyAllWindows()

#salvar imagem
def salva_imagem(img, nome):
	cv2.imwrite(nome, img, (cv2.IMWRITE_PXM_BINARY, 0))

#segmentação de uma imagem em texto e não texto
def segmentacao_imagem(img_original, kernel1, kernel2, kernel3):
	#quantidade de interações em cada uma das operações
	x = 1
	#dilatacao da imagem original com um elemento estruturante (kernel1)
	dilation = cv2.dilate(img_original,kernel1,iterations = x)
	cv2.imwrite("dilation_1.pbm", dilation, (cv2.IMWRITE_PXM_BINARY, 0))

	#erosao da imagem resultante com o mesmo elemento estruturante do passo (1);
	erosion = cv2.erode(dilation,kernel1,iterations = x)
	cv2.imwrite("erosion_2.pbm", erosion, (cv2.IMWRITE_PXM_BINARY, 0))


	#dilatacao da imagem original com um elemento estruturante (kernel2)
	dilation = cv2.dilate(img_original,kernel2,iterations = x)
	cv2.imwrite("dilation_3.pbm", dilation, (cv2.IMWRITE_PXM_BINARY, 0))

	#erosao da imagem resultante com o mesmo elemento estruturante do passo (3);
	erosion = cv2.erode(dilation,kernel2,iterations = x)
	cv2.imwrite("erosion_4.pbm", erosion, (cv2.IMWRITE_PXM_BINARY, 0))


	#aplicacao da interseccao (AND) dos resultados dos passos (2) e (4);
	img_1 = cv2.imread("erosion_2.pbm", 0)
	img_2 = cv2.imread("erosion_4.pbm", 0)

	img_and = cv2.bitwise_and(img_1, img_2, mask = None) 
	cv2.imwrite("erosion_5.pbm", img_and, (cv2.IMWRITE_PXM_BINARY, 0))

	#fechamento do resultado obtido no passo anterior com um elemento estruturante (kernel3)
	closing = cv2.morphologyEx(img_and, cv2.MORPH_CLOSE, kernel3)
	cv2.imwrite("closing_6.pbm", closing, (cv2.IMWRITE_PXM_BINARY, 0))

	#aplicacao de algoritmo para identificacao de componentes conexos sobre o resultado do
	#passo anterior;
	img_original = closing
	output = cv2.connectedComponentsWithStats(img_original, 4, cv2.CV_32S)
	# Destrinchando o output do connectedComponentsWithStats
	#quantiade de labels
	num_labels = output[0]
	#matriz de labels
	labels = output[1]
	#matriz de stat, xmin (left), ymin (top), width e height
	stats = output[2]
	# matriz dos centróides
	centroids = output[3]

	print ("possíveis palavras labels")
	print (num_labels)

	print ("Processando .....")

	# criação das matrizes
	# pp: pixels_preto para cada label
	# transicoes : transições em cada eixo x, y
	pp = np.zeros((num_labels,3))
	transicoes = np.zeros((num_labels,5))

	for i in range(num_labels):
		#The leftmost (x) coordinate which is the inclusive start of the
		#bounding box in the horizontal direction.
		x = stats[i][cv2.CC_STAT_LEFT]
		#The topmost (y) coordinate which is the inclusive start of the 
		#bounding box in the vertical direction.
		y = stats[i][cv2.CC_STAT_TOP]
		#The horizontal size of the bounding box
		w = stats[i][cv2.CC_STAT_WIDTH]
		#The vertical size of the bounding box
		h = stats[i][cv2.CC_STAT_HEIGHT]
		#The total area (in pixels) of the connected component
		area = stats[i][cv2.CC_STAT_AREA]

		#quantidade de pixels_preto
		pixels_pretos = 0
		#pixel inicial da transição e quantidade
		inicial = img_original[y][x]
		transicao_x = 0
		transicao_y = 0

		for j in range(w):
			for k in range(h):
				if (img_original[y+k][x+j] == 0):
					pixels_pretos = pixels_pretos + 1
				if (img_original[y+k][x+j] != inicial):
					transicao_y = transicao_y + 1
					inicial = img_original[y+k][x+j]

		inicial = img_original[y][x]

		for k in range(h):
			for j in range(w):
				if (img_original[y+k][x+j] != inicial):
					transicao_x = transicao_x + 1
					inicial = img_original[y+k][x+j]

		#pega a quantidade de pixels preto e a área
		pp[i][0] = pixels_pretos	
		pp[i][1] = area

		#transição vertical e horizontal
		transicoes[i][0] = transicao_y
		transicoes[i][1] = transicao_x

		img_original = cv2.rectangle(img_original,(x,y),(x+w,y+h),(255,255,255),2)

	img_original = cv2.bitwise_not(img_original)
	cv2.imwrite("componentes_conexo_7.pbm", img_original, (cv2.IMWRITE_PXM_BINARY, 0))

	#para cada retangulo envolvendo um objeto, calcule:
	#(a) razao entre o numero de pixels pretos e o numero total de pixels (altura × largura);

	for i in range(num_labels):
		pp[i][2] = pp[i][0]/pp[i][1]

	np.savetxt("pontos_preto.txt", pp, fmt="%s")

	#(b) razao entre o numero de transicoes verticais e horizontais branco para preto e o numero
	#total de pixels pretos;

	for i in range(num_labels):
		transicoes[i][2] = transicoes[i][0]/pp[i][0] # transicao_y/pixels_preto
		transicoes[i][3] = transicoes[i][1]/pp[i][0] # transicao_x/pixels_preto
		transicoes[i][4] = transicoes[i][2] + transicoes[i][3] #(transicao_x +transicao_x)/pixels_preto

	np.savetxt("transicoes.txt", transicoes, fmt="%s")

	n_palavras = 0
	for i in range(num_labels):
		if (transicoes[i][4]  >= 0.068) and (pp[i][2] >= 0.01 and pp[i][2] <= 0.62):
			n_palavras+=1

	print("palavras:")
	print(n_palavras)


#a imagem que foi passada como parâmetro
nome_imagem = sys.argv[1]

# criando a imagem (main image), negativando-a e tranformando em binária
img = cv2.imread(nome_imagem, 0)
img_original = cv2.bitwise_not(img)


#definição dos kernel para cada etapa do processo
kernel1 = cv2.getStructuringElement(cv2.MORPH_RECT, (100,1))#(largura, altura)
kernel2 = cv2.getStructuringElement(cv2.MORPH_RECT, (1,200))
kernel3 = cv2.getStructuringElement(cv2.MORPH_RECT, (30,1))

#operadores morfológicos para a segmentação de linha em bloco de palavras e envolvendo cada 
#palavrar por um retângulo
kernel4 = cv2.getStructuringElement(cv2.MORPH_RECT, (12,1)) #(largura, altura)
kernel5 = cv2.getStructuringElement(cv2.MORPH_RECT, (1,4))
kernel6 = cv2.getStructuringElement(cv2.MORPH_RECT, (12,1))

kernel7 = cv2.getStructuringElement(cv2.MORPH_RECT, (12,1)) #(largura, altura)
kernel8 = cv2.getStructuringElement(cv2.MORPH_RECT, (1,4))
kernel9 = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3))

segmentacao_imagem(img_original, kernel1, kernel2, kernel3)

print("Fim")